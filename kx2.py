#!/usr/bin/python3
"""
kx2.py - module to model serial port stuff with the KX2.

class KX2:
  use write(), readall() to operate the KX2

class KX2cmdtostring:
  use decode() to return human readable text from KX2.readall()

run module in python shell. then use term() to check commands.

"""

import sys # for KX2
import time  # for KX2
import traceback  # for KX2cmdtostring
from enum import Enum, auto # for decode_DS()
import select   # for term() _timeout_input()
from io import IOBase # for term() init file-like-object
import serial # for KX2

USER_PROMPT = "you>"
KX2_PROMPT = "kx2>"
PORT = "/dev/ttyUSB0"
BAUDRATE = 38400

class ItemOrDefault():
    def __init__(self, default, items):
        """utility class towrap parameter items, which is any subscriptable,
in a class to protect index from raise KeyError. Returns default when
asked for an index. not in items.

example:
    assert ItemOrDefault("none", {0: "A", 1: "B",})[3] == 'none'
"""
        self.default= default
        self.items = items

    def __getitem__(self, key):
        if key in self.items:
            return (self.items[key])
        return self.default

    def __repr__(self):
        return("default="+str(self.default)+", "+repr(self.items))


class KX2:
    """Manage serial port connectiom. write command. read responses to KX2"""
    def __init__(self, port=PORT, baudrate=BAUDRATE, timeout=0.2, debug=False):
        # timeout 100ms is documented by elecraft to be sufficient,
        # so let's use twice that just to be sure.
        self.debug = debug
        self.s = serial.Serial()
        self.s.port = port
        self.s.baudrate = baudrate
        self.s.timeout = timeout
        self.s.bytesize = serial.EIGHTBITS
        self.s.parity = serial.PARITY_NONE
        self.s.stopbits = serial.STOPBITS_ONE
        self.s.open()

    def write(self, cmd):
        "write(cmd) - send cmd to KX2.  cmd is a string."
        if not ';' == cmd[-1:]:
            cmd = cmd + ';' # terminate input string, without ; KX2 will wait forever
        if type("") == type(cmd):
            cmd = bytes(cmd, encoding='utf-8') # string() to bytes()
        if self.debug:
            print(USER_PROMPT, cmd)
        return self.s.write(cmd)

    def readall(self, size=99):
        """readall() - read and concatenate everything from the KX2
            returns type bytes() string.
        """
        # return self.s.read_until(expected, size)
        res = bytes("", encoding='utf-8')
        while self.s.in_waiting:
            res += self.s.read(size=size)
            # time.sleep(self.s.timeout)
        return res

    def docmd(self, cmd):
        "write() cmd to KX2, then return readall()"
        written_length = self.write(cmd)
        return self.readall(size=500)
        # buffer could be large if KX2 in AI2 mode and user operates
        # knobs and switches

class KX2cmdtostring:
    """parse the output of a KX2 command and format fields in a human readable form"""
    def __init__(self, debug=False):
        self.debug = debug
        self.commands = {
            'AG': self.decode_AG,
            'AI': self.decode_AI,
            'AN': self.decode_AN,
            'DS': self.decode_DS,
            'FA': self.decode_FA,
            'FB': self.decode_FB,
            'FR': self.decode_FR,
            'FT': self.decode_FT,
            'FW': self.decode_FW,
            'GT': self.decode_GT,
            'ID': self.decode_ID,
            'IF': self.decode_IF,
            'IS': self.decode_IS,
            'K2': self.decode_K2,
            'MG': self.decode_MG,
            'MD': self.decode_MD,
            'NB': self.decode_NB,
            'PA': self.decode_PA,
            'PC': self.decode_PC,
            'RA': self.decode_RA,
            'RG': self.decode_RG,
            'RO': self.decode_RO,
            'RT': self.decode_RT,
            'XT': self.decode_XT,
        }
        self.MODES = {0: "unknown", 8: "unknown",
               1: "LSB", 2: "USB", 3: "CW", 4: "FM", 5: "AM", 6: "RTTY",
               7: "CW reverse", 9: "RTTY reverse"}

    def _print_fnname(self, r):
        if self.debug:
            print(sys._getframe(1).f_code.co_name+ '("' + r + '")')

    def decode_AG(self, r):
        self._print_fnname(r)
        return "Audio Gain:" + str(int(r[2:]))

    def decode_AN(self, r):
        self._print_fnname(r)
        return "Antenna: " + str(int(r[2:]))

    def decode_AI(self, r):
        self._print_fnname(r)
        mode = r[-1:]
        try:
            mode_exp = {
                0: "no auto-info",
                1: "most changes, delayed 1 second",
                2: "all changes, immediately",
                3: "combination of mode 1 and 2",
            }[int(mode)]
        except KeyError:
            mode_exp = '?'
        return "decoded AI, mode " + mode + " " + mode_exp

    def decode_DS(self, r):
        self._print_fnname(r)
        res = 'Display: '
##        def _displaybits(v):
##            """ B7: Always 1
##                B6: 1=NB on
##                B5: 1=ANT2 selected
##                B4: 1=PREAMP on
##                B3: 1=ATT on
##                B2: 0=VFO A selected, 1=VFO B selected
##                B1: 1=RIT on
##                B0: 1=XIT on
##
##                ... but the document is wrong, for kx2 these bits don't match the display!
##            """
##            res = ""
##            if v & 0x80:
##                pass # always 1
##            if v & 0x40:
##                res += " NB"
##            if v & 0x20:
##                res += " ANT1"
##            if v & 0x10:
##                res += " preamp"
##            if v & 0x08:
##                res += " ATT"
##            if v & 0x04:
##                res += " VFO B"
##            else:
##                res += " VFO A"
##            if v & 0x02:
##                res += " RIT"
##            if v & 0x01:
##                res += " XIT"
##            return res

        # goddamn it.  reassemble 8 bit values from \x <hex> <hex>
        # state = clean -> \ ->  x { -> hex1 -> hex2 -> complete | -> clean }
        class ParseEscaped8bit(Enum):
            clean = auto()
            backslash = auto()
            x = auto()
            hex1 = auto()
            hex2 = auto()
            complete = auto()

        res += "'"
        cstate = ParseEscaped8bit.clean
        cord = 0
        for c in list(r)[2:-6]:
            #print("c='"+c+"', cstate="+str(cstate))
            if '\\' == c and ParseEscaped8bit.clean == cstate:
                cstate = ParseEscaped8bit.backslash
                continue
            if ParseEscaped8bit.backslash == cstate:
                if 'x' == c:
                    cstate = ParseEscaped8bit.x
                    continue
                else:
                    c = '\\' + c  # put back in \ before this character
                    cstate = ParseEscaped8bit.clean
            elif  ParseEscaped8bit.x == cstate and c.lower() in '0123456789abcdef':
                cord = int(c, base=16) << 4
                cstate = ParseEscaped8bit.hex1
                continue
            elif ParseEscaped8bit.hex1 == cstate:
                cord += int(c, base=16)
                cstate = ParseEscaped8bit.hex2

            if ParseEscaped8bit.hex2 == cstate:
                if cord > 127:
                    c = '.'  + chr(cord & 127)
                    cord = 0
                    cstate = ParseEscaped8bit.complete

            if ParseEscaped8bit.complete == cstate: # unnecessary
                cstate = ParseEscaped8bit.clean

            # finally!
            if ParseEscaped8bit.clean == cstate:
                # character fixups as per KX2 programmers' guide
                # list of tuples,
                # ( <displayed>, <actual> )
                for displayed, actual in [('@', ' '), ('<', 'L'), ('>', '-'),
                           ('K', 'H'), ('M', 'N'), ('Q', 'O'),
                           ('V', 'U'), ('W', 'I'), ('X', 'C_'),
                           ('Z', 'C'), ('[', 'R_'), ]:
                    if displayed == c:
                        c = actual
                res += c
            # end for()

        res += "'"
        return res

    def decode_FA(self, r):
        self._print_fnname(r)
        fa = int(r[2:])
        return "VFO A = " + str(fa / 1000.0)

    def decode_FB(self, r):
        self._print_fnname(r)
        fb = int(r[2:])
        return "VFO B = " + str(fb / 1000.0)

    def decode_FR(self, r):
        self._print_fnname(r)
        rxvfo = ItemOrDefault('?',{0: "A", 1: "B",})[int(r[2])]
        return "TX VFO:" + rxvfo

    def decode_FT(self, r):
        self._print_fnname(r)
        txvfo = ItemOrDefault('?',{0: "A", 1: "B",})[int(r[2])]
        return "TX VFO:" + txvfo

    def decode_FW(self, r):
        self._print_fnname(r)
        xxxx = r[2:6]
        if len(r) == 8:
            n = r[6]
            m = r[7]
        else:
            n = '?'
            m = '?'

        return "filter Bandwidth: " + xxxx + \
               ", filter number " + n + \
               ", audio mode " + m

    def decode_GT(self, r):
        self._print_fnname(r)
        tc = int(r[2:5])
        tcs = ItemOrDefault('?', {0: "off", 1: "on", 2: "slow", 4: "fast",})[tc]
        if len(r) == 6:
            s = ItemOrDefault('?', {0: "off", 1: "on",})[int(r[-1])]
        else:
            s = "(none)"

        return "AGC time constant:" + tcs + ", state: " + s

    def decode_ID(self, r):
        self._print_fnname(r)
        if "ID017" in str(r):
            return "connected to Elecraft"
        return "connected to unknown:" + r

    def decode_IF(self, r):
        self._print_fnname(r)
        ##format: IF[f]*****+yyyyrx*00tmvspb01*;
        ##[f] is frequency, 11 digits
        ##***** are spaces
        ##+yyyy is RIT offset, -9990 to +9990
        ##r RIT 0=off 1=on
        ##t XIT 0=off 1=on
        ##m is operating mode (MD command)
        ##v RX VFO 0=A 1=B
        ##s scan in progress  0=off 1=on
        ##p split mode  0=no 1=yes
        ##b response mode, generally 0, 1 if FT response is due to band change
        ##01 static

        res = "IF:"
        fa = int(r[2:2+11])
        res += " VFO=" + str(fa / 1000.0)
        ritoffset = r[2+11+5:2+11+5+7]
        res += " RIT=" + ritoffset

        # documentation is wrong, these values don't change with RIT XIT on off!!!
        # use RT and XT ibstead
        ##        rit = r[2+11+5+7+1]
        ##        res += " RIT " + {0:"off", 1:"on"}[int(rit)]
        ##        xit = r[2+11+5+7+2]
        ##        res += " XIT " + {0:"off", 1:"on"}[int(xit)]

        mode = r[2+11+5+7+3+1]
        res += " mode=" + ItemOrDefault('?', self.MODES)[int(mode)]

        vfo = r[2+11+5+7+3++1+1]
        res += " VFO " + ItemOrDefault('?', {"0":"A", "1":"B"})[vfo]

        scan = r[2+11+5+7+3+1+1+1]
        res += ", scan " + ItemOrDefault('?', {"0":"off", "1":"on"})[scan]

        split = r[2+11+5+7+3+1+1+1+1]
        res += ", split " + ItemOrDefault('?', {"0":"off", "1":"on"})[split]

        rspreason = r[2+11+5+7+3+1+1+1+1+1]
        res += ", sent-IF reason " + \
               ItemOrDefault('?', {"0":"normal", "1":"band change"})[rspreason]

        return res

    def decode_IS(self, r):
        self._print_fnname(r)
        return "undocumented IS: " + r

    def decode_K2(self, r):
        self._print_fnname(r)
        if "K2" in str(r):
            return "connected to KX2, command mode " + str(r)[-1:]
        return "connected to unknown:" + r

    # MDn; where n is 1 (LSB), 2 (USB), 3 (CW), 6 (RTTY), 7 (CW-REV), or 9 (RTTY-REV).
    def decode_MD(self, r):
        self._print_fnname(r)
        mode = int(r[2:].replace('$', '0')) # what does $ mean? undocumented
        ms = ItemOrDefault('?', self.MODES)[mode]
        return "Mode " + str(mode) + ": " + ms

    def decode_MG(self, r):
        self._print_fnname(r)
        return "Microphone Gain:" + str(int(r[2:]))

    def decode_NB(self, r):
        self._print_fnname(r)
        if len(r) == 4: # extended RSP format
            m = ItemOrDefault('?', {0: "low", 1: "high"})[int(r[3])]
            n = ItemOrDefault('?', {0: "off", 1: "NB1", 2: "NB2"})[int(r[2])]
        else: # basic RSP format
            m = "(none)"
            n = ItemOrDefault('?', {0: "off", 1: "on",})[int(r[2])]
        return "Noise Blanker: " + n + ", threshold " + m

    def decode_PA(self, r):
        self._print_fnname(r)
        return "Preamp: " + ItemOrDefault('?', {0: "off", 1: "on", })[int(r[2])]

    def decode_PC(self, r):
        self._print_fnname(r)
        if len(r) == 6:
            poran = int(r[5])
        else:
            poran = -1
        po = r[2:5]
        if poran == 0:

            po = "%2.1f" % (float(po) / 10.0)
        return "Power Output Level: " + po + \
               " watts, range: " + \
               ItemOrDefault('?', {-1: "missing", 0: "low", 1: "high", })[poran]

    def decode_RA(self, r):
        self._print_fnname(r)
        return "Attenuator: " + ItemOrDefault('?', {0: "off", 1: "on", })[int(r[2])]

    def decode_RG(self, r):
        self._print_fnname(r)
        return "undocumented RG: " + str(int(r[2:]))

    def decode_RO(self, r):
        self._print_fnname(r)
        return "RIT offset: " + str(r[2:])

    def decode_RT(self, r):
        self._print_fnname(r)
        rit = r[2:]
        return "RIT: " + ItemOrDefault('?', {0: "off", 1: "on", })[int(r[2])]

    def decode_XT(self, r):
        self._print_fnname(r)
        xit = r[2]
        return "XIT: " + ItemOrDefault('?', {0: "off", 1: "on", })[int(xit)]

    def parsecmd(self, r):
        # use regex
        # ignore "b'"
        # look for trailing ;, print 'incomplete' if nissing
        # pick out the rest of the line, minus:
        # ignore trailing ';'
        # look for two A-Z 0-9, that is the command
        # deal with \\xnn -> 0xnn, convert to int()?  no... let decoders do it

        r = str(r).replace("b'", "")
        lame = r[:2]
        # print("lame='" + str(lame) + "'")
        if lame in self.commands.keys():
            try:
                return self.commands[lame](r)
            except:
                print("oops!  exception from", str(self.commands[lame]))
                exc_type, exc_value, exc_traceback = sys.exc_info()
                traceback.print_exception(exc_type, exc_value, exc_traceback)
                return 'broken: ' + r
        else:
            if not lame:
                return lame
            elif "'" == lame:
                return ""
            return "no decoder: " + lame


    def decode(self, cmds):
        out = ""
        for r in str(cmds).split(';'):
            if not r or "'" == r:
                continue
            if out:
                out += '\n'
            out += self.parsecmd(r)
        return out


def setup(port=PORT, baudrate=BAUDRATE, debug=True):
    """Open KX2 and send a few commands to prove the serial connection."""
    kx2 = KX2(port=port, baudrate=baudrate, debug=debug)
    if kx2:
        if kx2.debug:
            print("KX2 opened")
    else:
        print("open failed")
        assert False

    _cmd = KX2cmdtostring(kx2.debug)
    for cmd in ["ID", "K2", "AI"]:
        res = kx2.docmd(cmd)
        if kx2.debug:
            print(KX2_PROMPT, res)
            print(_cmd.decode(res))
            print()

def term(port=PORT, baudrate=BAUDRATE, init=None, debug=True):
    """term() - send user input to KX2, print return from KX2.
use debug { cmd | kx2 | off } to control decode output.
use listen to poll quickly for KX2 messages.  Ctrl+C to exit back to prompt.
use testall to run commands for all decoders.
use ctrl-C to quit."""
    # pass in port ("/dev/ttyUSB0") and baudrate if needed.
    # init can be a string (run directly), a function (called),
    # or a file (read and run)

    def _timeout_input(prompt, timeout, default=""):
        inputs, outputs, errors = select.select([sys.stdin], [], [], timeout)
        return (0, sys.stdin.readline().strip()) if inputs else (-1, default)

    def _get_cmds():
        if "idlelib" in str(type(sys.stdin)): # class idlelib.run.PseudoInputFile
            # idle object doesn't work for select()
            cmds = input(USER_PROMPT+" ")
        else: # try to timeout and poll for kx2 output
            print(USER_PROMPT, end=" ", flush=True)
            res = -1
            while res:
                res, cmds = _timeout_input(prompt="", timeout=10.0)
                # TBD is good enough, timeout 10 seconds then poll for output?
                # cmds == '' after ctrl+D, not empty otherwise.  ctrl+C gets exception raised.
                if res: # timeout or other error
                    #  look for some queued output, print it
                    out = _cmd.decode(kx2.readall(size=300))
                    if out:
                        print(out)
                        print(USER_PROMPT, end=" ", flush=True)
                        # TBD: rescue anything user typed before interruption
        return cmds

    def _listen():
        print("... listening to KX2.  ctrl+c to exit")
        try:
            while True:
                time.sleep(0.5) # TBD use better polling delay
                out = _cmd.decode(kx2.readall(size=300))
                if out:
                    print(out)
        except KeyboardInterrupt:
            print("ctrl+c")

    def _docmd(cmd):
        res = kx2.docmd(cmd)
        print(KX2_PROMPT, res)
        print(_cmd.decode(res))
        print()

    def _testall(cmds):
        for cmd in cmds:
            _docmd(cmd)

    print("Entering", term.__doc__)
    kx2 = KX2(port=port, baudrate=baudrate, debug=debug)
    assert kx2
    _cmd = KX2cmdtostring(debug=debug)

    if init: print ("init not implemented") # TBD
##    if type(init) == type("string"):
##        _docmd(init)
##    elif type(init) == type(lambda x: x):
##        init()
##    elif isinstance(init, IOBase):
##        for l in readlines(init):
##            _docmd(l)

    while True:
        try:
            cmd = _get_cmds()
            if "debug" in cmd:
                kx2.debug = "kx2" in cmd or "on" in cmd
                _cmd.debug = "cmd" in cmd or "on" in cmd
                continue
            if "testall" in cmd:
                _testall(_cmd.commands.keys())
                continue
            if "help" in cmd:
                print(term.__doc__)
                print("Commands with decoder functions")
                print("  ", ", ".join(_cmd.commands.keys()))
                continue
            if "listen" in cmd:
                _listen()
                continue

            _docmd(cmd)
        except KeyboardInterrupt:
            print("ctrl+c")
            break
        except EOFError:
            print("EOF")
            break
    # end pf term()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--port", default=PORT,
                        help="name of serial port for KX2")
    parser.add_argument("--baudrate", default=BAUDRATE,
                        help="baudrate of serial port for KX2")
    parser.add_argument("--debug", default=True,
                        help="trace functions and status talking to the KX2. { off | on }")
##    parser.add_argument("--init", action="count", default=None,
##                        help="string of commands, or name of file, to run before cmd prompt")
    args = parser.parse_args()
#    print("parsed args - ", args)
    setup(port=args.port, baudrate=args.baudrate, debug=args.debug)
    term(port=args.port, baudrate=args.baudrate, debug=args.debug)
